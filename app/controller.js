const data = require("./data");
class Controller {
    async messages() {
        return new Promise((resolve, _) => resolve(data));
    }
}
module.exports = Controller;