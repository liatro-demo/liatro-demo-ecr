//app.js
const http = require("http");
const Msg = require("./controller");
const { getReqData } = require("./utils");
const PORT = process.env.PORT || 5000;
const server = http.createServer(async (req, res) => {
    if (req.url === "/api/msg" && req.method === "GET") {
        const msg = await new Msg().messages();
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(JSON.stringify(msg));
    }
    if (req.url === "/api/msg2" && req.method === "GET") {
        const msg = await new Msg().messages();
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(JSON.stringify(msg));
    }
});
server.listen(PORT, () => {
    console.log(`server started on port: ${PORT}`);
});