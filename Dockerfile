FROM node:lts-alpine
WORKDIR /app
COPY app/ .
EXPOSE 5000

CMD [ "node","app.js" ]